package actors;

import java.awt.Image;

import javax.swing.ImageIcon;

import Oggetti.ScreenObject;

public class Mushrooms extends Actors implements Runnable{

	private Image imgFunghi;
	private ImageIcon icoFunghi;
	
	private final int PAUSE = 15;
	private int dxFunghi;
	
	public Mushrooms(int X, int Y) {
		super(X, Y,27 , 30);
		super.setVerso_destra(true);
		super.setIn_movimento(true);
		this.dxFunghi = 1;
		this.icoFunghi = new ImageIcon(getClass().getResource("/images/funghiAD.png"));
		this.imgFunghi = icoFunghi.getImage();
		
		Thread chronoFunghi = new Thread(this);
		chronoFunghi.start();
	}

	//getters
	public Image getImgFunghi() {
		return imgFunghi;
	}

	
	//setters
	
	
	//metodi
	
	public void muoviti(){
		if(super.isVerso_destra() == true){this.dxFunghi =1;}
		else{this.dxFunghi= -1; }
		super.setX(super.getX()+this.dxFunghi);
		
	}
	
	@Override
	public void run() {
		try{Thread.sleep(20);}
		catch(InterruptedException e){}
		
		while(true){
			if(this.vivo == true){
				this.muoviti();
				try{Thread.sleep(PAUSE);}
				catch(InterruptedException e){}
			}
		}
	}
	
	public void contact (ScreenObject obj){
		if(super.contactAvanti(obj) == true && this.isVerso_destra() == true){
			super.setVerso_destra(false);
			this.dxFunghi = -1;
		}else if (super.contactIndietro(obj)==true && this.isVerso_destra() ==false){
			super.setVerso_destra(true);
			this.dxFunghi = 1;
		}
	}
	
	public void contact (Actors pers){
		if(super.contactAvanti(pers) == true && this.isVerso_destra() == true){
			super.setVerso_destra(false);
			this.dxFunghi = -1;
		}else if (super.contactIndietro(pers)==true && this.isVerso_destra() ==false){
			super.setVerso_destra(true);
			this.dxFunghi = 1;
		}
	}
	
	  public Image muore(){		
			String str;	
			ImageIcon ico;
			Image img;
	        if(this.isVerso_destra() == true){str = "/images/funghiED.png";}
	        else{str = "/images/funghiEG.png";}
	        ico = new ImageIcon(getClass().getResource(str));
	        img = ico.getImage();
			return img;
		}
	

	
}
