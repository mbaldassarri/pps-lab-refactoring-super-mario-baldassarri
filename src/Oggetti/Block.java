package Oggetti;

import javax.swing.ImageIcon;

public class Block extends ScreenObject {
	
	public Block(int X, int Y) {
		
		super(X, Y, 30 ,30);
		super.icoObj = new ImageIcon(getClass().getResource("/images/Blocco.png"));
		super.imgObj = super.icoObj.getImage();
	}

	
}
