package game;

import Oggetti.ScreenObject;
import actors.*;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import Oggetti.Block;
import Oggetti.Piece;
import Oggetti.Tunnel;

@SuppressWarnings("serial")
public class Platform extends JPanel{
	
	private ImageIcon backgroundIco ;
	private Image imgbackground ;
	private Image imgbackground2 ;
	
	private ImageIcon casteloico ;
	private Image castelo ;
	private ImageIcon startico ;
	private Image start ;
	
	//dimension con estremita a sinistra del background
	private int x1 ; //background 1
	private int x2 ; // altri background 
	private int mov ; // indice per il movimento che verra incrementato o decrementato
	private int xPos;  // variabile per riperire gli elementi del gioco sul l'asse X
	private int ySol ; // altezza del pavimento
	private int hauteurPlafond ;
	
	public Mario mario;
	
	//funghi del gioco
	public Mushrooms mushrooms;
	
	//turtle in the game
	public Turtle turtle ;
	
	
	// tunello nel gioco
	public Tunnel tunnel1;
	public Tunnel tunnel2;
	public Tunnel tunnel3;
	public Tunnel tunnel4;
	public Tunnel tunnel5;
	public Tunnel tunnel6;
	public Tunnel tunnel7;
	public Tunnel tunnel8;
	
	// bloco del gioco
	public Block block1;
	public Block block2;
	public Block block3;
	public Block block4;
	public Block block5;
	public Block block6;
	public Block block7;
	public Block block8;
	public Block block9;
	public Block block10;
	public Block block11;
	public Block block12;
	
	public Piece piece1;
	public Piece piece2;
	public Piece piece3;
	public Piece piece4;
	public Piece piece5;
	public Piece piece6;
	public Piece piece7;
	public Piece piece8;
	public Piece piece9;
	public Piece piece10;
	
	// bandiera e castello di fine
	private ImageIcon icoBandiera ;
	private Image imgBandiera ;	
	private ImageIcon icoCastelloF ;
	private Image imgCastelloF ;

	private ArrayList<ScreenObject> tabobj ; //tabella per salvare tutti gli oggeti
	private ArrayList<Piece> tabPieces ; // table with all the piece
	
	//costrutore
	public Platform(){
		
		super();
		this.x1 = -50 ;
		// -50 + width = 750
		this.x2 = 750 ;
		this.mov = 0;
		this.xPos = -1 ;
		this.ySol = 293;
		this.hauteurPlafond = 0;
		backgroundIco = new ImageIcon(getClass().getResource("/images/background.png"));
		this.imgbackground = this.backgroundIco.getImage();
		this.imgbackground2 = this.backgroundIco.getImage();
		casteloico = new ImageIcon(getClass().getResource("/images/castelloIni.png"));
		this.castelo = this.casteloico.getImage();
		startico = new ImageIcon(getClass().getResource("/images/start.png"));
		this.start = this.startico.getImage();
		mario = new Mario(300, 245);
		mushrooms = new Mushrooms(800, 263);
		turtle = new Turtle(950, 243);
		// position of all the object 
		
		tunnel1 = new Tunnel(600, 230);
		tunnel2 = new Tunnel(1000, 230);
		tunnel3 = new Tunnel(1600, 230);
		tunnel4 = new Tunnel(1900, 230);
		tunnel5 = new Tunnel(2500, 230);
		tunnel6 = new Tunnel(3000, 230);
		tunnel7 = new Tunnel(3800, 230);
		tunnel8 = new Tunnel(4500, 230);
		
		block1 = new Block(400, 180);
		block2 = new Block(1200, 180);
		block3 = new Block(1270, 170);
		block4 = new Block(1340, 160);
		block5 = new Block(2000, 180);
		block6 = new Block(2600, 160);
		block7 = new Block(2650, 180);
		block8 = new Block(3500, 160);
		block9 = new Block(3550, 140);
		block10 = new Block(4000, 170);
		block11 = new Block(4200, 200);
		block12 = new Block(4300, 210);
		
		piece1 = new Piece(402, 145);
		piece2 = new Piece(1202, 140);
		piece3 = new Piece(1272, 95);
		piece4 = new Piece(1342, 40);
		piece5 = new Piece(1650, 145);
		piece6 = new Piece(2650, 145);
		piece7 = new Piece(3000, 135);
		piece8 = new Piece(3400, 125);
		piece9 = new Piece(4200, 145);
		piece10 = new Piece(4600, 40);
		
		
		this.icoCastelloF = new ImageIcon(getClass().getResource("/images/castelloF.png"));
		this.imgCastelloF = icoCastelloF.getImage();
		
		this.icoBandiera = new ImageIcon(getClass().getResource("/images/bandiera.png"));
		this.imgBandiera = icoBandiera.getImage();
		
		tabobj = new ArrayList<ScreenObject>();
		
		this.tabobj.add(tunnel1);
		this.tabobj.add(tunnel2);
		this.tabobj.add(tunnel3);
		this.tabobj.add(tunnel4);
		this.tabobj.add(tunnel5);
		this.tabobj.add(tunnel6);
		this.tabobj.add(tunnel7);
		this.tabobj.add(tunnel8);
		
		this.tabobj.add(block1);
		this.tabobj.add(block2);
		this.tabobj.add(block3);
		this.tabobj.add(block4);
		this.tabobj.add(block5);
		this.tabobj.add(block6);
		this.tabobj.add(block7);
		this.tabobj.add(block8);
		this.tabobj.add(block9);
		this.tabobj.add(block10);
		this.tabobj.add(block11);
		this.tabobj.add(block12);
		
		tabPieces = new ArrayList<Piece>();			
		this.tabPieces.add(this.piece1);
		this.tabPieces.add(this.piece2);
		this.tabPieces.add(this.piece3);
		this.tabPieces.add(this.piece4);
		this.tabPieces.add(this.piece5);
		this.tabPieces.add(this.piece6);
		this.tabPieces.add(this.piece7);
		this.tabPieces.add(this.piece8);
		this.tabPieces.add(this.piece9);
		this.tabPieces.add(this.piece10);
	
		//schermo listener
		this.setFocusable(true);
		this.requestFocusInWindow();
		
		//collegamento con la classe keyboard
		this.addKeyListener(new Keyboard());
	}
	
	//getters

	public int getySol() {
		return ySol;
	}

	public int getHauteurPlafond() {
		return hauteurPlafond;
	}

	public int getMov() {
		return mov;
	}
	
	public int getxPos() {
		return xPos;
	}
		
	//setters
	public void setX2(int x2) {
		this.x2 = x2;
	}

	public void setySol(int ySol) {
		this.ySol = ySol;
	}

	public void setHauteurPlafond(int hauteurPlafond) {
		this.hauteurPlafond = hauteurPlafond;
	}

	public void setxPos(int xPos) {
		this.xPos = xPos;
	}

	public void setMov(int mov) {
		this.mov = mov;
	}
	
	public void setX1(int x) {
		this.x1 = x;
	}
	
	// metodo per gestire la permanenza del fondo (si muove il background )	
	public void movimento_di_fondo(){
		/* per bloccare la posizione il ritorno verso la sinistra con il castello poi si aggiorna il valore
		 di xpos dentro la classe keyboard affinche non sia mai negativo */
		if(this.xPos >= 0 && this.xPos <= 4600){
			this.xPos = this.xPos + this.mov ;
			this.x1 = this.x1 - this.mov ;  // si muove la schermato per dare l'impressione che mario camina
			this.x2 = this.x2 - this.mov ;  // si muove la schermato per dare l'impressione che mario camina
		}
		 
		//condizioni per aggiornamento del background a l'infinito  	
		if(this.x1 == -800){this.x1 = 800;}		// dove il background 1 finisce si mette il 2 (a destra )	
		else if (this.x2 == -800){this.x2=800;}	// dove il background 2 finisce si mette il 1 (a destra )	
		else if (this.x1 == 800){this.x1=-800;}	// dove il background 1 finisce si mette il 2 (a sinistra )			
		else if (this.x2 == 800){this.x2= -800;}// dove il background 2 finisce si mette il 1 (a sinistra )	
	}

	// disegno dei component	
	public void paintComponent(Graphics g){		 
		super.paintComponent(g);
		Graphics g2 = (Graphics2D)g; 		
		//detezione contact con l'oggetto piu vicino di lui
		for (int i = 0 ; i<tabobj.size() ; i++){
			//contact di mario con gli oggetti
			if(this.mario.vicino(this.tabobj.get(i)))
				this.mario.contact(this.tabobj.get(i));
			//contact di mario con gli oggetti
			if(this.mushrooms.vicino(this.tabobj.get(i)))
				this.mushrooms.contact(this.tabobj.get(i));
			//contact turtle
			if(this.turtle.vicino(this.tabobj.get(i)))
				this.turtle.contact(this.tabobj.get(i));			
		}
		
		//detection with the piece 
		for(int i=0 ; i <tabPieces.size() ; i++){
			if(this.mario.contactPiece(this.tabPieces.get(i))){
				Audio.playSound("/audio/money.wav");
				this.tabPieces.remove(i);
			}
		}
		
		//contact fra turtle e funghi
		if(this.mushrooms.vicino(turtle)){this.mushrooms.contact(turtle);}
		if(this.turtle.vicino(mushrooms)){this.turtle.contact(mushrooms);}
		if(this.mario.vicino(mushrooms)){this.mario.contact(mushrooms);}
		if(this.mario.vicino(turtle)){this.mario.contact(turtle);}
		
		// spostamento oggetti fissi
		this.movimento_di_fondo(); 
		if(this.xPos >= 0 && this.xPos <= 4600){
			for (int i = 0 ; i<tabobj.size() ; i++){
				tabobj.get(i).spostamenti();
			}
			
			for(int i=0 ; i <tabPieces.size() ; i++){
				this.tabPieces.get(i).spostamenti();
			}
			
			this.mushrooms.spostamenti();
			this.turtle.spostamenti();
		}
		
		g2.drawImage(this.imgbackground, this.x1, 0, null);
		g2.drawImage(this.imgbackground2, this.x2, 0, null); // disegno images di fondo2
		g2.drawImage(this.castelo, 10 - this.xPos, 95, null);
		g2.drawImage(this.start, 220-this.xPos, 234, null);
		
		// disegno di tutti gli oggetti	
			for (int i = 0 ; i<tabobj.size() ; i++){
				g2.drawImage(this.tabobj.get(i).getImgObj(),this.tabobj.get(i).getX() ,
						this.tabobj.get(i).getY(), null);
			}
		
		//design of piece's image 
		for(int i=0 ; i <tabPieces.size() ; i++){
			g2.drawImage(this.tabPieces.get(i).muoviti(), this.tabPieces.get(i).getX(),
					this.tabPieces.get(i).getY(), null);
		}
		// disegno castello fine e bandiera
		g2.drawImage(this.imgBandiera, 4650 -this.xPos, 115, null);
		g2.drawImage(this.imgCastelloF, 4850 - this.xPos, 145, null);
		
		//disegno di mario
		if(this.mario.isSalto() == true)
			g2.drawImage(this.mario.Salto(), this.mario.getX(), this.mario.getY(), null);
		else g2.drawImage(this.mario.walk("mario", 25), this.mario.getX(), this.mario.getY(), null);
			
		//disegno del fungho
		if(this.mushrooms.isVivo() == true )
			g2.drawImage(this.mushrooms.walk("funghi", 45), this.mushrooms.getX(), this.mushrooms.getY(), null);
		else
			g2.drawImage(this.mushrooms.muore(), this.mushrooms.getX(), this.mushrooms.getY() +20 , null);
		
		//disegno turtle
		if(this.turtle.isVivo() == true)
			g2.drawImage(this.turtle.walk("turtle", 45), this.turtle.getX(), this.turtle.getY(), null);
		else g2.drawImage(this.turtle.muore(), this.turtle.getX(), this.turtle.getY() + 30, null);
	}
	
}
