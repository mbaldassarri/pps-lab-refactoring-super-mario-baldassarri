package game;

import javax.swing.JFrame;

public class Main {

	public  static Platform scene ;
	public static void main(String[] args) {
		JFrame window = new JFrame("super_pilar");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setSize(700 ,360);
		window.setLocationRelativeTo(null);
		window.setResizable(true);
		window.setAlwaysOnTop(true);
		scene = new Platform();
		window.setContentPane(scene);
		window.setVisible(true);
		Thread chrono = new Thread(new Refresh());
		chrono.start();
	}

}
